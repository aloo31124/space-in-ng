export * from './lib/core.module';

//服務
export { Test } from'./lib/booking/services/test.service';
export { BookingService } from'./lib/booking/services/booking.service';

//模組
export { Booking } from'./lib/booking/models/booking';

