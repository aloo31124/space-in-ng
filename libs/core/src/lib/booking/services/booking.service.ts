import { Injectable } from '@angular/core';
import { Booking } from '../models/booking';

import { 
    Firestore,
    collection,
    collectionData,
    addDoc
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BookingService {


    constructor(
        private firestore: Firestore
    ) { }

    post(booking: Booking) {      
        const collectionInstance = collection(this.firestore, 'UsersTest');
        addDoc(collectionInstance, booking)
            .then(() => {
                //console.log("success!! :)")
                alert("post success 2 ! selectDate : " + booking.selectDate + " ,"  + booking.selectTime);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getAll() {        
        const collectionInstance = collection(this.firestore, 'UsersTest');
        collectionData(collectionInstance)
            .subscribe((data) => {
                console.log(data);
            });
        return collectionInstance;
    }

    
}