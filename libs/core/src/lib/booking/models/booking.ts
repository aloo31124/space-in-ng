//預約空間欄位

export class Booking {

    mail: string;
    selectDate: string;
    selectTime: string;
    //startTime: string;
    //endTime: string;
    bookingType: string;

    constructor(data: Booking){
        this.mail = data.mail;        
        this.selectDate = data.selectDate;
        this.selectTime = data.selectTime;
        //this.startTime = data.startTime;
        //this.endTime = data.endTime;
        this.bookingType = data.bookingType;
    }
}