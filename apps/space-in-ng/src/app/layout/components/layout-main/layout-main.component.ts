import { Component } from '@angular/core';
import { Location } from '@angular/common'

@Component({
  selector: 'space-in-ng-layout-main',
  templateUrl: './layout-main.component.html',
  styleUrls: ['./layout-main.component.scss'],
})
export class LayoutMainComponent {
  

  headerImageUrl = "";
  isMainHeader = true;

  constructor(
    private location: Location
  ) {
    const urlContent = "assets/header/";

    if(window.innerWidth > 1500) {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-2000px.png":"secondary/bar-secondary-2000px.png");
    } else if( 500 <= window.innerWidth && window.innerWidth <= 1500) {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-1000px.png":"secondary/bar-secondary-1000px.png");
    } else {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-400px.png":"secondary/bar-secondary-400px.png");
    }    
    console.log(this.headerImageUrl );
  }

  back() {
    this.location.back();
  }

}
