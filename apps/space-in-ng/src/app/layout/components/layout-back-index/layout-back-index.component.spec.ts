import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutBackIndexComponent } from './layout-back-index.component';

describe('LayoutBackIndexComponent', () => {
  let component: LayoutBackIndexComponent;
  let fixture: ComponentFixture<LayoutBackIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LayoutBackIndexComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LayoutBackIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
