import { Component } from '@angular/core';

@Component({
  selector: 'space-in-ng-layout-menu',
  templateUrl: './layout-menu.component.html',
  styleUrls: ['./layout-menu.component.scss'],
})
export class LayoutMenuComponent {}
