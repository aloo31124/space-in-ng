import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { take } from 'rxjs';

@Component({
  selector: 'space-in-ng-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {

  headerImageUrl = "";
  isMainHeader = false;
  isBackIndex = false;
  isHiddenBacke = false;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {

    const urlContent = "assets/header/";

    if(window.innerWidth > 1500) {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-2000px.png":"secondary/bar-secondary-2000px.png");
    } else if( 500 <= window.innerWidth && window.innerWidth <= 1500) {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-1000px.png":"secondary/bar-secondary-1000px.png");
    } else {
      this.headerImageUrl 
        = urlContent + (this.isMainHeader?"main/bar-main-400px.png":"secondary/bar-secondary-400px.png");
    }    
    console.log(this.headerImageUrl );
  }


  ngOnInit(): void {    

    /**
     * 路由, 檢查導航列是否為指定狀態
     */
    this.activatedRoute.firstChild?.data
      .pipe(take(1))
      .subscribe((data) => {
        if (data) {
          if (data['isBackIndex'] === true) {
            this.isBackIndex = true;
          } else if (data['isHiddenBacke'] === true) {
            this.isHiddenBacke = true
          }
        }
      });
  }

  back() {
    if(this.isBackIndex){ // 回首頁
      this.router.navigate(["index"]);
    } else {
      this.location.back();
    }
  }

}
