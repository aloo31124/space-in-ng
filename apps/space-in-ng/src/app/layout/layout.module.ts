import { NgModule } from '@angular/core';
import { LayoutComponent } from './components/layout/layout.component';
import { RouterModule } from '@angular/router';
import { LayoutMainComponent } from './components/layout-main/layout-main.component';
import { LayoutBackIndexComponent } from './components/layout-back-index/layout-back-index.component';

@NgModule({
  declarations: [
    LayoutComponent,
    LayoutMainComponent,
    LayoutBackIndexComponent,
  ],
  imports: [RouterModule],
  exports: [LayoutComponent],
})
export class LayoutModule {}
