import { Routes } from "@angular/router";
import { LayoutComponent } from "./layout/components/layout/layout.component";
import { LayoutMainComponent } from "./layout/components/layout-main/layout-main.component";
import { LayoutBackIndexComponent } from "./layout/components/layout-back-index/layout-back-index.component";
// Auth Guard

export const AppRoutes: Routes = [
    {
        path: "",
        component: LayoutMainComponent,
        children: [             
            {
                path: "", //登入
                loadChildren: () => import("./pages/login/login.module").then(m => m.LoginModule)
            },
            {
                path: "index", //首頁
                loadChildren: () => import("./pages/index/index.module").then(m => m.IndexModule)
            },

        ]

    },
    {
        path: "",
        component: LayoutComponent,
        //canActivate: [],
        children: [
            {
                path: "menu",
                loadChildren: () => import("./pages/menu/menu.module").then(m => m.MenuModule)
            },
            {
                path: "booking",
                loadChildren: () => import("./pages/booking/booking.module").then(m => m.BookingModule)
            },
            {
                path: "map",
                loadChildren: () => import("./pages/map/map.module").then(m => m.MapModule)
            },
            /*
            {
                path: "review-booking",
                loadChildren: () => import("./pages/review-booking/review-booking.module").then(m => m.ReviewBookingModule),
                //data: { "isBackIndex": true }
            },
            */
            {
                path: "review-room",
                loadChildren: () => import("./pages/review-room/review-room.module").then(m => m.ReviewRoomModule)
            },
            {
                path: "pay",
                loadChildren: () => import("./pages/payment/payment.modules").then(m => m.PaymentModule)
            },
        ]
    },    
    {
        // post 後實在不知道怎麼寫，先寫這個 ="=
        path: "",
        component: LayoutBackIndexComponent,
        children: [             
            {
                path: "review-booking",
                loadChildren: () => import("./pages/review-booking/review-booking.module").then(m => m.ReviewBookingModule),
            },
        ]

    },
];



/* 
@NgModule({})
export class AppRouting { }
 */