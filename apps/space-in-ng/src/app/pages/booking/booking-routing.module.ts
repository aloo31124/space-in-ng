import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CalendarPickerComponent } from "./calendar-picker/calendar-picker.component";
import { ClockPickerComponent } from './clock-picker/clock-picker.component';
import { BookingSelectorComponent } from './booking-selector/booking-selector.component';
import { BookingFormComponent } from './booking-form/booking-form.component';

export const bookingRoutes: Routes = [
    {
        path: "calendar", // 日期選擇頁面 子路由
        component: CalendarPickerComponent
    },
    {
        path: "clock", //  時間選擇頁面 子路由
        component: ClockPickerComponent
    },
    {
        path: "selector", // 教室、座位預約選擇頁面 子路由
        component: BookingSelectorComponent
    },
    {
        path: "form", // 預約空間表單 子路由
        component: BookingFormComponent
    },
];

@NgModule({
    //declarations: [],
    imports: [
        RouterModule.forChild(bookingRoutes) // 子路由綁定?
    ],
    exports: [
        RouterModule
    ]
})
export class BookingRoutingModule {}