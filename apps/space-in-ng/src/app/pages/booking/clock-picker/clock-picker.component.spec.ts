import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ClockPickerComponent } from './clock-picker.component';

describe('ClockPickerComponent', () => {
  let component: ClockPickerComponent;
  let fixture: ComponentFixture<ClockPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClockPickerComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ClockPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
