import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'space-in-ng-booking-selector',
  templateUrl: './booking-selector.component.html',
  styleUrls: ['./booking-selector.component.scss'],
})
export class BookingSelectorComponent implements OnInit {

  selectDate = "";
  selectTime = "";

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // 提取日期参数
    this.activatedRoute.queryParams.subscribe(params => {
      this.selectDate = params['selectDate'];
      this.selectTime = params['selectTime'];
    });
  }

  selectForm(bookingType: string) {
    
    const navigationExtras: NavigationExtras = {
      queryParams: {
        selectDate: this.selectDate,
        selectTime: this.selectTime,
        bookingType: bookingType
      }
    };

    this.router.navigate(["booking/form"], navigationExtras);
  }

}
