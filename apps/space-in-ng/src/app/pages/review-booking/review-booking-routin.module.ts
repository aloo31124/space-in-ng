import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReviewBookingCalendarComponent } from "./review-booking-calendar/review-booking-calendar.component";
import { ReviewBookingFormComponent } from "./review-booking-form/review-booking-form.component";

export const MenuRouters: Routes = [
    {
        path: "review-booking-calendar",
        component: ReviewBookingCalendarComponent
    },
    {
        path: "review-booking-form",
        component: ReviewBookingFormComponent
    }
];

@NgModule({
    //declarations: [ ],
    imports: [
        RouterModule.forChild(MenuRouters) // 將 review-booking 之子路由 綁定
    ],
    exports: [
        RouterModule
    ]
})
export class ReviewBookingRoutingModule {}