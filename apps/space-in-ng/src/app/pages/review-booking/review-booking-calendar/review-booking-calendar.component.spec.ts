import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReviewBookingCalendarComponent } from './review-booking-calendar.component';

describe('ReviewBookingCalendarComponent', () => {
  let component: ReviewBookingCalendarComponent;
  let fixture: ComponentFixture<ReviewBookingCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewBookingCalendarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ReviewBookingCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
