import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'space-in-ng-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent {
  
  constructor(
    private router : Router
  ) {}

  clickBooking() {
    this.router.navigate(["/booking/calendar"]);
  }

  clickReviewBooking() {
    this.router.navigate(["/review-booking/review-booking-calendar"]);    
  }

  clickReviewRoomOverview()  {
    this.router.navigate(["/review-room/review-room-overview"]);
  }

  clickPayment() {
    this.router.navigate(["/pay/pay-plan-list"]);
  }

}
