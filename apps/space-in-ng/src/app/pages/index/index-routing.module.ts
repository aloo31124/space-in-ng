import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { IndexComponent } from "./index/index.component";

export const IndexRoutes: Routes = [
    {
        path: "", //子路由
        component: IndexComponent
    }
];

@NgModule({
    //declarations: [],
    imports: [
        RouterModule.forChild(IndexRoutes) // 子路由綁定?
    ],
    exports: [
        RouterModule
    ]
})
export class IndexRoutingModule {}