import { NgModule } from "@angular/core";
import { IndexComponent } from "./index/index.component";
import { IndexRoutingModule } from "./index-routing.module";

@NgModule({
    declarations: [ 
        IndexComponent 
    ],
    imports: [
        IndexRoutingModule // 匯入 index路由模組
    ],
    //exports: [ IndexComponent ] // 不將 index元件 匯出
})
export class IndexModule {}