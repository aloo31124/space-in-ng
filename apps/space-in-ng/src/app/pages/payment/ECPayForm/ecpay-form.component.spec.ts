import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ECPayFormComponent } from './ecpay-form.component';

describe('ECPayFormComponent', () => {
  let component: ECPayFormComponent;
  let fixture: ComponentFixture<ECPayFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ECPayFormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ECPayFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
