import { Component } from '@angular/core';

@Component({
  selector: 'space-in-ng-ecpay-form',
  templateUrl: './ecpay-form.component.html',
  styleUrls: ['./ecpay-form.component.scss'],
})
export class ECPayFormComponent {}
