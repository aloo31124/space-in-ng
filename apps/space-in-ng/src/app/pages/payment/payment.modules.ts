import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common'; //[ngClass]
import { PaymentRoutingModule } from "./payment-routing.module";
import { PaidPlanListComponent } from "./paidPlanList/paid-plan-list.component";
import { ECPayFormComponent } from "./ECPayForm/ecpay-form.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
    declarations: [
        PaidPlanListComponent,
        ECPayFormComponent,
    ],
    imports: [
        HttpClientModule,
        PaymentRoutingModule,
        CommonModule,
    ]
    //exports: []
})
export class PaymentModule {}