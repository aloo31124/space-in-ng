import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'space-in-ng-paid-plan-list',
  templateUrl: './paid-plan-list.component.html',
  styleUrls: ['./paid-plan-list.component.scss'],
})
export class PaidPlanListComponent implements OnInit {

  @ViewChild('paymentPage') paymentPageEl!: ElementRef;

  constructor(private http: HttpClient) {}

  async ngOnInit() {
    // 取得 HTML 內容
    const paymentPageHtml = await this.fetchPaymentPageHtml();

    // 將 HTML 內容設定給 paymentPageEl
    this.paymentPageEl.nativeElement.innerHTML = paymentPageHtml;
    

  }

  private async fetchPaymentPageHtml(): Promise<string> {
    
    // 發送 HTTP 請求到綠界 API
    const apiUrl = 'https://httprequesttest-querqokzna-uc.a.run.app';
    this.http
      .get(apiUrl)
      .subscribe(
        x => {
          console.log("success!");
          console.log(x);
          return x;
          
        },
        error => {
          console.log(" error 2 ! : " + error.error.text);
          this.paymentPageEl.nativeElement.innerHTML = error.error.text;
          return error.error.text;          
        }
      );
      return "yoyoyo! ";
  }



}


