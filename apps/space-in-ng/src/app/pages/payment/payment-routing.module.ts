import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PaidPlanListComponent } from "./paidPlanList/paid-plan-list.component";
import { ECPayFormComponent } from "./ECPayForm/ecpay-form.component";

export const MenuRouters: Routes = [
    {
        path: "pay-plan-list",
        component: PaidPlanListComponent
    },
    {
        path: "ecpay",
        component: ECPayFormComponent
    }
];

@NgModule({
    //declarations: [ ],
    imports: [
        RouterModule.forChild(MenuRouters) // 將 review-booking 之子路由 綁定
    ],
    exports: [
        RouterModule
    ]
})
export class PaymentRoutingModule {}