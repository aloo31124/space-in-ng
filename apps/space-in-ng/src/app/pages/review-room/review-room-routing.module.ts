import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReviewRoomDetailComponent } from "./review-room-detail/review-room-detail.component";
import { ReviewRoomOverviewComponent } from "./review-room-overview/review-room-overview.component";

export const MenuRouters: Routes = [
    {
        path: "review-room-detail",
        component: ReviewRoomDetailComponent
    },
    {
        path: "review-room-overview",
        component: ReviewRoomOverviewComponent
    }
];

@NgModule({
    //declarations: [ ],
    imports: [
        RouterModule.forChild(MenuRouters) // 將 review-booking 之子路由 綁定
    ],
    exports: [
        RouterModule
    ]
})
export class ReviewRoomRoutingModule {}