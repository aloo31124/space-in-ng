import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReviewRoomOverviewComponent } from './review-room-overview.component';

describe('ReviewRoomOverviewComponent', () => {
  let component: ReviewRoomOverviewComponent;
  let fixture: ComponentFixture<ReviewRoomOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReviewRoomOverviewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ReviewRoomOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
