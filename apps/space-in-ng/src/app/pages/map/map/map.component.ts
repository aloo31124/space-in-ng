import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { GoogleMap } from '@capacitor/google-maps';


@Component({
  selector: 'space-in-ng-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent {

  //https://capacitorjs.com/docs/apis/google-maps
  @ViewChild("map") mapRef!: ElementRef;
  map!: GoogleMap;
  

  constructor() {
    setTimeout(async () => { await this.createMap() }, 100);
    //this.createMap();
  }
  

  async createMap() {    
    this.map = await GoogleMap.create({
      id: "my-app",
      apiKey: "AIzaSyDHf9UYhCx_3eI_f5kuXLdYuvFDnOINhm8",
      config: {
        center: {
          lat: 22.62,
          lng: 120.30,
        },
        zoom: 5,
      },
      element: this.mapRef.nativeElement
    })
  }

}
