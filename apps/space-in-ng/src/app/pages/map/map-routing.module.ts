import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MapComponent } from "./map/map.component";

const mapRoutes: Routes = [
    {
        path: "", // 路由子路徑
        component: MapComponent
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(mapRoutes) // 將子路徑綁定
    ],
    exports: [
        RouterModule
    ]
})
export class MapRoutingModule { }