import { NgModule } from "@angular/core";
import { MapComponent } from "./map/map.component";
import { MapRoutingModule } from './map-routing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
    declarations: [
        MapComponent
    ],
    imports: [
        MapRoutingModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
    /* 
    exports: [
        LoginModuleRouting
    ] */
})
export class MapModule { }