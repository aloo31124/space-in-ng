import { Component } from '@angular/core';

@Component({
  selector: 'space-in-ng-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {}
