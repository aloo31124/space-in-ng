import { NgModule } from "@angular/core";
import { MenuComponent } from "./menu/menu.component";
import { MenuRoutingModule } from "./menu-routing.module";

@NgModule({
    declarations: [
        MenuComponent
    ],
    imports: [
        MenuRoutingModule
    ]
    //exports: []
})
export class MenuModule {}