import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MenuComponent } from "./menu/menu.component";

export const MenuRouters: Routes = [
    {
        path: "", // menu/ 子路由
        component: MenuComponent
    }
];

@NgModule({
    //declarations: [ MenuComponent],
    imports: [
        RouterModule.forChild(MenuRouters) // 將 menu之子路由 綁定
    ],
    exports: [
        RouterModule
    ]
})
export class MenuRoutingModule {}