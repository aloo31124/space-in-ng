/* eslint-disable @typescript-eslint/no-empty-function */
import { Component } from '@angular/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { Router } from '@angular/router';


@Component({
  selector: 'space-in-ng-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  
  constructor(    
    private router: Router
  ) {    
    
    alert("login start! yo!");
    GoogleAuth.initialize({ grantOfflineAccess: true });
  }

  async clickSignIn() {
    try {      
      const user = await GoogleAuth.signIn();
      console.log(user);
      alert(user.email);
      if (user) {
        this.router.navigate(["index"]);
      }
    } catch (error) {
        console.log(error);
        alert(error);
    }
  }


}
