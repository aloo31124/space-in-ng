import { NgModule } from "@angular/core";
import { LoginComponent } from "./login/login.component";
import { LoginModuleRouting } from "./login-routing.module";

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        LoginModuleRouting
    ],/* 
    exports: [
        LoginModuleRouting
    ] */
})
export class LoginModule { }