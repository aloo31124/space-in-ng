import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";

const loginRoutes: Routes = [
    {
        path: "", // 路由子路徑
        component: LoginComponent
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(loginRoutes) // 將子路徑綁定
    ],
    exports: [
        RouterModule
    ]
})
export class LoginModuleRouting { }