import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
//import { appRoutes } from './app.routes';
import { AppRoutes } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, // Browser 相關資源模組
    RouterModule.forRoot(
      AppRoutes,
      { useHash: true } // url 增加 #
    ),
    LayoutModule // 須將 layout模組引入， 方可讓 app route 內 index 於 layout > outer-let 導向
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
