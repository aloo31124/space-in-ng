import { Component } from '@angular/core';

@Component({
  selector: 'space-in-ng-root',
  template: `
   <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'space-in-ng';
}
