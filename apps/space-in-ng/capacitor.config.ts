import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {  

  plugins: {
    PushNotifications: {
      presentationOptions: ["badge", "sound", "alert"],
    },
    GoogleAuth: {
      scopes: ["profile", "email"],
      serverClientId: "328066296243-5opekodfa93rria1e8utcql4rkrbvktq.apps.googleusercontent.com",
      forceCodeForRefreshToken: false
    }
  },

  appId: 'com.spacein',
  appName: 'space in',
  webDir: '..//..//dist//apps//space-in-ng',
  bundledWebRuntime: false
};

export default config;
